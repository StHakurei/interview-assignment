# The purpose of this program is read the system log file (data_set) and analysis error message
# to extra useful information, which includes "deviceName", "processId", "processName",
# "description", "timeWindow", "numberOfOccurrence", then generate JSON format report
# and POST to target log server (https://foo.com/bar).
import json
import hashlib
import requests

LOG_FILE = "data_set"
LOG_SERVER_URL = "https://foo.com/bar"
LINE_BREAKER = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
]

errorCount = 0
errorRecord = {}


def getSHA256(input):
    object = hashlib.sha256()
    object.update(input.encode("utf-8"))
    return object.hexdigest()


def breakCheck(value):
    return value not in LINE_BREAKER


def parseMessage(message):
    global errorCount, errorRecord
    if "error" in message:
        sets = message.split(" ", 4)
        # Get time and hostname.
        timestamp = sets[0] + " " + sets[1] + " " + sets[2]
        hostname = sets[3]

        # Get message body contains process information and description.
        messageBody = sets[4]
        processInfo = messageBody.split(":", 1)[0]
        description = messageBody.split(":", 1)[1]

        # Get process name and id.
        processName = processInfo.split("[")[0]
        processId = processInfo.split("[")[1].split("]")[0]

        errorCount = errorCount + 1
        # Generate record id to identify same erroe message from one process.
        recordId = getSHA256(hostname + processName + description)
        if recordId not in errorRecord:
            errorRecord.update(
                {
                    recordId: {
                        "deviceName": hostname,
                        "processId": processId,
                        "processName": processName,
                        "description": description,
                        "timeWindow": [timestamp, timestamp],
                        "numberOfOccurrence": 1,
                    }
                }
            )
        else:
            timeWindow = errorRecord.get(recordId).get("timeWindow")
            timeWindow[1] = timestamp
            count = errorRecord.get(recordId).get("numberOfOccurrence") + 1
            errorRecord[recordId]["timeWindow"] = timeWindow
            errorRecord[recordId]["numberOfOccurrence"] = count


# Open log file.
with open(LOG_FILE, encoding="utf-8") as logFile:
    record = ""
    num = 1
    for line in logFile:
        record = line
        num = num + 1

        if breakCheck(line.split(" ")[0]) and (num > 1):
            record = record + line
        else:
            parseMessage(record)
            record = ""

print(json.dumps(errorRecord, sort_keys=True, indent=4))
print("Found error records: " + str(errorCount))
print("Send error message to target log server - " + LOG_SERVER_URL)
# Below code will send filtered log in Json format.
# send = requests.post(LOG_SERVER_URL, json.dumps(errorRecord))
# print(f"Response Code: {send.status_code}")
